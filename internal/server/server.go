// Copyright © VNG Realisatie 2021
// Licensed under EUPL v1.2

package server

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"net/http"

	"html/template"

	oidc "github.com/coreos/go-oidc/v3/oidc"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
)

var templates = template.Must(template.ParseGlob("./templates/*.html"))

type Concierge struct {
	Endpoint          string `json:"endpoint"`
	CABundleData      string `json:"caBundleData"`
	AuthenticatorName string `json:"authenticatorName"`
	AuthenticatorType string `json:"authenticatorType"`
}

type UpstreamIdentityProvider struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type Cluster struct {
	Name                     string                   `json:"name"`
	Description              string                   `json:"description"`
	Concierge                Concierge                `json:"concierge"`
	ClientID                 string                   `json:"clientID"`
	Issuer                   string                   `json:"issuer"`
	RequestAudience          string                   `json:"requestAudience"`
	UpstreamIdentityProvider UpstreamIdentityProvider `json:"upstreamIdentityProvider"`
}

type Server struct {
	oauth2Config *oauth2.Config
	oidcProvider *oidc.Provider
	oidcVerifier *oidc.IDTokenVerifier
	cookieStore  *sessions.CookieStore
	clusters     []Cluster
}

func NewServer(oidcIssuer string, oidcClientID string, oidcSecret string, oidcRedirectURL string, sessionKey string, clusters []Cluster) (*Server, error) {
	ctx := context.Background()
	oidcProvider, err := oidc.NewProvider(ctx, oidcIssuer)
	if err != nil {
		return nil, err
	}

	oauth2Config := &oauth2.Config{
		ClientID:     oidcClientID,
		ClientSecret: oidcSecret,
		RedirectURL:  oidcRedirectURL,
		Endpoint:     oidcProvider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID, "groups", "email"},
	}

	oidcVerifier := oidcProvider.Verifier(&oidc.Config{ClientID: oidcClientID})

	cookieStore := sessions.NewCookieStore([]byte(sessionKey))

	return &Server{
		oauth2Config: oauth2Config,
		oidcProvider: oidcProvider,
		oidcVerifier: oidcVerifier,
		cookieStore:  cookieStore,
		clusters:     clusters,
	}, nil
}

func (s *Server) ListenAndServe(address string) error {
	http.HandleFunc("/", s.handleLogin)
	http.HandleFunc("/callback", s.handleCallback)

	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	return http.ListenAndServe(address, nil)
}

func (s *Server) handleLogin(w http.ResponseWriter, r *http.Request) {
	log.Println("handling login")

	session, _ := s.cookieStore.Get(r, "oidc") // omit error, as a new session will be created

	state, err := randString(16)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	session.Values["state"] = state
	err = sessions.Save(r, w)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, s.oauth2Config.AuthCodeURL(state), http.StatusFound)
}

func (s *Server) handleCallback(w http.ResponseWriter, r *http.Request) {
	log.Println("handling OIDC callback")

	session, err := s.cookieStore.Get(r, "oidc")
	if err != nil {
		log.Println("could not retrieve session", err)
		renderError(w, http.StatusBadRequest, "Bad request", "Could not retrieve session.")
		return
	}

	sessionState, ok := session.Values["state"]
	if !ok {
		log.Println(err)
		renderError(w, http.StatusBadRequest, "Bad request", "Could not retrieve state from session.")
		return
	}

	queryState, ok := r.URL.Query()["state"]
	if !ok {
		log.Println(err)
		renderError(w, http.StatusBadRequest, "Bad request", "Could not retrieve session state from query parameters.")
		return
	}

	if sessionState.(string) != queryState[0] {
		log.Println(err)
		renderError(w, http.StatusBadRequest, "Bad request", "Invalid session state.")
		return
	}

	ctx := context.Background()
	oauth2Token, err := s.oauth2Config.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		log.Println(err)
		renderError(w, http.StatusBadRequest, "Bad request", "Could not exchange code for token.")
		return
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		log.Println(err)
		renderError(w, http.StatusInternalServerError, "Internal server error", "Could not extract ID token from OAuth2 token.")
		return
	}

	idToken, err := s.oidcVerifier.Verify(ctx, rawIDToken)
	if err != nil {
		log.Println(err)
		renderError(w, http.StatusBadRequest, "Bad request", "Could not verify ID token.")
		return
	}

	var claims map[string]interface{}
	if err := idToken.Claims(&claims); err != nil {
		log.Println(err)
		renderError(w, http.StatusInternalServerError, "Internal server error", "Error occured while unmarshalling ID token.")
		return
	}

	encodedClaims, err := json.MarshalIndent(claims, "", "    ")
	if err != nil {
		log.Println(err)
		renderError(w, http.StatusInternalServerError, "Internal server error", "Error occured while marshalling JSON.")
		return
	}

	encodedClusters, err := json.Marshal(s.clusters)
	escapedEncodedClusters := template.JS(encodedClusters)
	if err != nil {
		log.Println(err)
		renderError(w, http.StatusInternalServerError, "Internal server error", "Error occured while marshalling JSON.")
		return
	}

	renderKubectl(w, string(encodedClaims), escapedEncodedClusters)
}

func randString(nByte int) (string, error) {
	b := make([]byte, nByte)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}

func renderKubectl(w http.ResponseWriter, claims string, clusters template.JS) {
	data := struct {
		Claims   string
		Clusters template.JS
	}{
		Claims:   claims,
		Clusters: clusters,
	}

	if err := templates.ExecuteTemplate(w, "kubectl.html", data); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}

func renderError(w http.ResponseWriter, statusCode int, title string, message string) {
	w.WriteHeader(http.StatusBadRequest)

	data := struct {
		Title   string
		Message string
	}{
		Title:   title,
		Message: message,
	}

	if err := templates.ExecuteTemplate(w, "error.html", data); err != nil {
		log.Fatalln("could not render error template", err)
	}
}
