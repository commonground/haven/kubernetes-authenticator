FROM golang:1.22.5-alpine AS build

WORKDIR /go/src/pinniped-authenticator

COPY go.mod go.sum ./
COPY vendor vendor
COPY cmd cmd
COPY internal internal

ARG VERSION=unknown

RUN addgroup -S -g 1001 haven && adduser -S -D -H -G haven -u 1001 haven
RUN go build -v \
      -ldflags "-w -s -X 'main.version=${VERSION}'" \
      -o /go/bin/pinniped-authenticator \
      cmd/pinniped-authenticator/main.go

FROM alpine:3.20.2

WORKDIR /usr/local/bin

COPY --from=build /go/bin/pinniped-authenticator pinniped-authenticator
COPY --from=build /etc/passwd /etc/group /etc/

COPY static static
COPY templates templates

USER haven
ENTRYPOINT ["/usr/local/bin/pinniped-authenticator"]
