// Copyright © VNG Realisatie 2021
// Licensed under EUPL v1.2

package main

import (
	"log"
	"os"

	cli "github.com/jawher/mow.cli"
	"github.com/spf13/viper"
	"gitlab.com/commonground/haven/pinniped-authenticator/internal/server"
)

var version = "unknown"

var options struct {
	ConfigFile       string
	ListenAddress    string
	SecretKey        string
	OIDCClientID     string
	OIDCClientSecret string
	OIDCIssuerURL    string
	OIDCCallbackURL  string
}

func main() {
	app := cli.App("pinniped-authenticator", "A friendly UI for developers so they can easily access multiple clusters")

	app.StringPtr(&options.ConfigFile, cli.StringOpt{Name: "config-file", Value: "config.yaml", EnvVar: "CONFIG_FILE", Desc: "location of the configuration file"})
	app.StringPtr(&options.ListenAddress, cli.StringOpt{Name: "listen-address", Value: ":8080", EnvVar: "LISTEN_ADDRESS", Desc: "address for the authenticator to listen on"})
	app.StringPtr(&options.SecretKey, cli.StringOpt{Name: "secret-key", Value: "", EnvVar: "SECRET_KEY", Desc: "secret that is used for signing sessions"})
	app.StringPtr(&options.OIDCClientID, cli.StringOpt{Name: "oidc-client-id", Value: "", EnvVar: "OIDC_CLIENT_ID", Desc: "OIDC client ID"})
	app.StringPtr(&options.OIDCClientSecret, cli.StringOpt{Name: "oidc-client-secret", Value: "", EnvVar: "OIDC_CLIENT_SECRET", Desc: "OIDC client secret"})
	app.StringPtr(&options.OIDCIssuerURL, cli.StringOpt{Name: "oidc-issuer-url", Value: "", EnvVar: "OIDC_ISSUER_URL", Desc: "OIDC issuer URL"})
	app.StringPtr(&options.OIDCCallbackURL, cli.StringOpt{Name: "oidc-callback-url", Value: "", EnvVar: "OIDC_CALLBACK_URL", Desc: "OIDC callback URL"})

	app.Action = func() {
		initConfig()
		startServer()
	}

	app.Run(os.Args)
}

func initConfig() {
	viper.SetConfigFile(options.ConfigFile)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalln("Could not read config file", err)
	}

	log.Println("Using config file:", viper.ConfigFileUsed())
}

func startServer() {
	var clusters []server.Cluster
	viper.UnmarshalKey("clusters", &clusters)

	server, err := server.NewServer(
		options.OIDCIssuerURL,
		options.OIDCClientID,
		options.OIDCClientSecret,
		options.OIDCCallbackURL,
		options.SecretKey,
		clusters,
	)

	if err != nil {
		log.Fatalln("could not construct server", err)
	}

	log.Println("starting pinniped-authenticator on", options.ListenAddress)

	err = server.ListenAndServe(options.ListenAddress)
	if err != nil {
		log.Fatalln(err)
	}
}
